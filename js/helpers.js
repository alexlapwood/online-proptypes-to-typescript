function interfaceFromProps(props) {
  const interface = Object.keys(props)
    .map(key => {
      const isRequired = props[key].isRequired;
      return `${key}${isRequired ? "?" : ""}: ${props[key]}`;
    })
    .join(";\n");
  return `interface MyProps {\n${interface};\n}`;
}
