class PropType {
  constructor(propString) {
    this.propString = propString;
    this.isRequired = this.toString();
  }

  toString() {
    return this.propString;
  }
}

class PropTypes {
  static get any() {
    return new PropType("any");
  }
  static get array() {
    return new PropType("any[]");
  }
  static get bool() {
    return new PropType("boolean");
  }
  static get element() {
    return new PropType("React.ReactNode");
  }
  static get func() {
    return new PropType("() => any");
  }
  static get node() {
    return new PropType("React.ReactNode");
  }
  static get number() {
    return new PropType("number");
  }
  static get object() {
    return new PropType("{}");
  }
  static get string() {
    return new PropType("string");
  }
  static get symbol() {
    return new PropType("symbol");
  }

  static arrayOf(type) {
    switch (type.toString()) {
      case "boolean":
      case "number":
      case "string":
      case "symbol":
        return new PropType(`${type}[]`);
      default:
        return new PropType(`Array<${type}>`);
    }
  }

  static oneOf(types) {
    return new PropType(`"${types.join('" | "')}"`);
  }

  static objectOf(type) {
    return new PropType(`{[index: string]: ${type}}`);
  }

  static oneOfType(types) {
    return new PropType(types.join(" | "));
  }

  static shape(props) {
    return new PropType(interfaceFromProps(props));
  }
}
