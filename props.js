document.getElementById("props").oninput = event => {
  try {
    const props = eval(`(${event.target.value})`);
    const interface = interfaceFromProps(props);
    const formattedInterface = prettier.format(interface, {
      parser: "typescript",
      plugins: prettierPlugins
    });

    document.getElementById("interface").innerText = formattedInterface;

    console.log(formattedInterface);
  } catch (error) {
    document.getElementById("interface").innerText = error;
  }
};
